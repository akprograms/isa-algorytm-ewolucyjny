﻿using EvoAlgorithmLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISA_Algorytm_Ewolucyjny
{
    public partial class TestsForm : Form
    {
        //public delegate List<EvolutionaryAlgorithm> CallEvoAlg(int lowerRange, int upperRange, int precision, int populationSize, int generationsNumber, bool functionIsMaximizing, double crossoverChance, double mutationChance);
        //public CallEvoAlg CallEvolutionaryAlgorithm { get; set; }

        private List<BackgroundWorker> Workers = new List<BackgroundWorker>();
        private List<ProgressBar> Bars = new List<ProgressBar>();
        private List<EvolutionaryAlgorithm> EvoAlg = new List<EvolutionaryAlgorithm>();
        //private List<CallEvoAlg> CallEvolutionaryAlgorithm = new List<CallEvoAlg>();

        private int processesCompleted = 0;

        public TestsForm(int LowerRange, int UpperRange, int Precision, bool IsMaximizing) // , CallEvoAlg CEA
        {
            InitializeComponent();

            for (int i = 0; i < 11; i++)
            {
                Workers.Add(new BackgroundWorker());
                Workers[i].WorkerReportsProgress = true;
                //CallEvolutionaryAlgorithm.Add(new CallEvoAlg(CEA));
            }

            {
                Bars.Add(progressBar1);
                Bars.Add(progressBar2);
                Bars.Add(progressBar3);
                Bars.Add(progressBar4);
                Bars.Add(progressBar5);
                Bars.Add(progressBar6);
                Bars.Add(progressBar7);
                Bars.Add(progressBar8);
                Bars.Add(progressBar9);
                Bars.Add(progressBar10);
                Bars.Add(progressBar11);
            }


            int repeats = 100;

            int[] numberOfIndividuals = new int[11]; numberOfIndividuals[0] = 30; // start 30 koniec 80 skok 5 || 11 wartosci
            int[] numberOfGenerations = new int[11]; numberOfGenerations[0] = 50; // start 50 koniec 150 skok 10 || 11 wartosci
            double[] crossoverProbability = new double[9]; crossoverProbability[0] = 0.5; // start 0.5 koniec 0.9 skok 0.05 || 9 wartosci
            double[] mutationProbability = new double[21]; mutationProbability[0] = 0.0001; // start 0.0001 koniec 0.01 skok 0.0005 || 21 wartosci

            for (int i = 1; i < numberOfIndividuals.Length; i++) numberOfIndividuals[i] = numberOfIndividuals[0] + 5 * i;
            for (int i = 1; i < numberOfGenerations.Length; i++) numberOfGenerations[i] = numberOfGenerations[0] + 10 * i;
            for (int i = 1; i < crossoverProbability.Length; i++) crossoverProbability[i] = crossoverProbability[0] + 0.05 * i;
            for (int i = 1; i < mutationProbability.Length; i++) mutationProbability[i] = 0.0005 * i;

            //List<Thread> calculationThreads = new List<Thread>();

            StringBuilder sb = new StringBuilder();

            Workers.ForEach(w => w.DoWork += delegate(object sender, DoWorkEventArgs e) {
                BackgroundWorker worker = sender as BackgroundWorker;
                int highestPercentageReached = 0;
                List<TestData> TestData = new List<TestData>();
                string filePath = "Test" + (Workers.IndexOf(w)+1) + ".csv";

                for (int t = 0; t < numberOfGenerations.Length; t++)
                {
                    for (int pk = 0; pk < crossoverProbability.Length; pk++)
                    {
                        for (int pm = 0; pm < mutationProbability.Length; pm++)
                        {
                            List<double> TempGeneration = new List<double>();
                            int lowerRange = LowerRange;
                            int upperRange = UpperRange;
                            int precision = Precision;
                            int populationSize = numberOfIndividuals[Workers.IndexOf(w)];
                            int generationsNumber = numberOfGenerations[t];
                            bool functionIsMaximizing = IsMaximizing;
                            double crossoverChance = crossoverProbability[pk];
                            double mutationChance = mutationProbability[pm];

                            for (int r = 0; r < repeats; r++)
                            {
                                // 207 900 powtórzeń
                                // Console.WriteLine("Powtarzam " + i + " dla: numberOfIndividuals = " + NumberOfIndividuals + ", numberOfGenerations = " + NumberOfGenerations[t] + ", crossoverProbability = " + CrossoverProbability[pk] + ", mutationProbability = " + MutationProbability[pm]);

                                // algorytm ewolucyjny
                                //EvoAlg = CallEvolutionaryAlgorithm.ElementAt(Workers.IndexOf(w))(LowerRange, UpperRange, Precision, numberOfIndividuals[Workers.IndexOf(w)], numberOfGenerations[t], IsMaximizing, crossoverProbability[pk], mutationProbability[pm]);
                                //EvoAlg = startAlgorithm(LowerRange, UpperRange, Precision, numberOfIndividuals[Workers.IndexOf(w)], numberOfGenerations[t], IsMaximizing, crossoverProbability[pk], mutationProbability[pm]);

                                List<EvolutionaryAlgorithm> EvoAlg = new List<EvolutionaryAlgorithm>();
                                double accuracy = Math.Pow(10, precision);
                                int rangeLength = upperRange > 0 ? upperRange - lowerRange : Math.Abs(lowerRange - upperRange);
                                double tmp = rangeLength * accuracy + 1;
                                int chromosomesNumber = (int)Math.Ceiling(Math.Log(tmp, 2));

                                Random rand = new Random();

                                for (int actualGeneration = 0; actualGeneration < generationsNumber; actualGeneration++)
                                {
                                    EvoAlg.Clear();
                                    for (int i = 1; i <= populationSize; i++)
                                    {
                                        double xReal;
                                        if (actualGeneration > 0)
                                        {
                                            xReal = TempGeneration.ElementAt(i - 1);
                                        }
                                        else
                                        {
                                            xReal = Math.Round(rand.NextDouble() * (upperRange - lowerRange) + lowerRange, precision);
                                        }

                                        double Fx = Utils.doEvaluation(xReal);

                                        EvoAlg.Add(new EvolutionaryAlgorithm
                                        {
                                            Number = i,
                                            XReal = xReal,
                                            EvaluationFunction = Fx
                                        });
                                    }

                                    // selekcja
                                    double fext;
                                    if (functionIsMaximizing)
                                    {
                                        fext = EvoAlg.Min(ind => ind.EvaluationFunction);
                                    }
                                    else
                                    {
                                        fext = EvoAlg.Max(ind => ind.EvaluationFunction);
                                    }

                                    foreach (var individual in EvoAlg)
                                    {
                                        individual.FitnessFunction = Utils.fitness(individual.EvaluationFunction, fext, precision, functionIsMaximizing);
                                    }

                                    double sumOfFitnessed = EvoAlg.Sum(f => f.FitnessFunction);
                                    foreach (var individual in EvoAlg)
                                    {
                                        individual.Probability = Utils.countProbability(individual.FitnessFunction, sumOfFitnessed);
                                    }

                                    List<double> probabilities = EvoAlg.Select(s => s.Probability).ToList();
                                    for (int i = 0; i < populationSize; i++)
                                    {
                                        EvoAlg.ElementAt(i).Distribution = Utils.doDistribution(probabilities, i);
                                    }

                                    List<double> weights = EvoAlg.Select(s => s.Distribution).ToList();
                                    foreach (var individual in EvoAlg)
                                    {
                                        individual.Random = rand.NextDouble();
                                        individual.XRealAfterSelection = EvoAlg.Select(x => x.XReal).ElementAt(Utils.select(weights, individual.Random));
                                    }

                                    // krzyzowanie
                                    foreach (var individual in EvoAlg)
                                    {
                                        if (rand.NextDouble() < crossoverChance)
                                        {
                                            individual.ParentXReal = individual.XRealAfterSelection;
                                            individual.ParentXBin = Utils.real2bin(individual.XRealAfterSelection, rangeLength, lowerRange, chromosomesNumber);
                                        }
                                        else
                                        {
                                            individual.ParentXBin = "---";
                                            individual.XRealAfterCrossover = individual.XRealAfterSelection;
                                        }
                                    }

                                    List<EvolutionaryAlgorithm> crossIndividuals = EvoAlg.Where(i => i.ParentXReal != 0).ToList();
                                    Utils.doCrossover(crossIndividuals, chromosomesNumber, lowerRange, rangeLength, precision);

                                    // mutacja 
                                    Utils.doMutation(EvoAlg, chromosomesNumber, lowerRange, rangeLength, precision, mutationChance);

                                    TempGeneration = EvoAlg.Select(x => x.XRealAfterMutiation).ToList();
                                    
                                    TestData.Add(new TestData
                                    {
                                        Max = EvoAlg.Select(x => x.EvaluationFunction).Max(),
                                        Avg = EvoAlg.Select(x => x.EvaluationFunction).Average()
                                    });
                                    
                                }

                                // zapisanie wyników do pliku
                                //Console.WriteLine("Cos");
                                //worker.ReportProgress((int)Math.Ceiling((double)((t+1) * (pk+1) * (pm+1) * (r+1)) / 207900 * 100)); // [(t+1) * (pk+1) * (pm+1) * (r+1)] / 207900 * 100
                            }

                            string avg = TestData.Select(x => x.Avg).Average().ToString();
                            string max = TestData.Select(x => x.Max).Max().ToString();

                            sb.Append(numberOfIndividuals[Workers.IndexOf(w)] + ";");
                            sb.Append(numberOfGenerations[t] + ";");
                            sb.Append(crossoverProbability[pk] + ";");
                            sb.Append(mutationProbability[pm] + ";");
                            sb.Append(avg + ";");
                            sb.AppendLine(max + ";");

                            File.AppendAllText(filePath, sb.ToString());

                            sb.Clear();
                            TestData.Clear();

                            //(int)((float)n / (float)numberToCompute * 100);
                            int percentcomplete = (int)((float)((t + 1) * (pk + 1) * (pm + 1)) / (float)2079 * 100);
                            if(percentcomplete > highestPercentageReached)
                            {
                                highestPercentageReached = percentcomplete;
                                worker.ReportProgress(percentcomplete);
                            }
                            
                        }
                    }
                }
            });

            Workers.ForEach(w => w.ProgressChanged += delegate(object sender, ProgressChangedEventArgs e) {
                Bars.ElementAt(Workers.IndexOf(w)).Value = e.ProgressPercentage;
            });

            Workers.ForEach(w => w.RunWorkerCompleted += delegate (object sender, RunWorkerCompletedEventArgs e)
            {
                if (e.Error != null)
                {
                    MessageBox.Show(e.Error.Message);
                }
                else if(e.Cancelled)
                {
                    MessageBox.Show("Operacja anulowana");
                }
                else
                {
                    processesCompleted++;
                    if (processesCompleted > 10)
                    {
                        MessageBox.Show("Obliczenia zakończone! Wyniki zapisano w plikach Test1-11.csv w lokalizacji programu");
                    }
                }
            });

            // 2 286 900 powtórzeń
            for (int n = 0; n < numberOfIndividuals.Length; n++)
            {
                Workers.ElementAt(n).RunWorkerAsync();
            }
        }
    }
}
