﻿using EvoAlgorithmLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISA_Algorytm_Ewolucyjny
{
    public partial class SummaryForm : Form
    {
        public List<PopulationSummary> LastPopulation = new List<PopulationSummary>();

        public SummaryForm(List<ChartData> ChartData, List<EvolutionaryAlgorithm> EvoAlg, int rangeLength, int lowerRange, int chromosomesNumber)
        {
            InitializeComponent();
            //StringBuilder sb = new StringBuilder();
            //string filePath = "Podsumowanie.csv";
            //string filePathB = "Chart.csv";

            chartDataBindingSource.DataSource = ChartData;
            chartDataBindingSource.ResetBindings(true);

            /*
            foreach (var item in ChartData)
            {
                sb.Append(item.Number + ";");
                sb.Append(item.Min + ";");
                sb.Append(item.Avg + ";");
                sb.AppendLine(item.Max + ";");
                File.AppendAllText(filePathB, sb.ToString());
                sb.Clear();
            }
            */

            foreach (var individual in EvoAlg)
            {
                if(LastPopulation.Select(x => x.XReal).Contains(individual.XRealAfterMutiation))
                {
                    LastPopulation.Single(x => x.XReal.Equals(individual.XRealAfterMutiation)).Percent += 1.0 / EvoAlg.Count * 100;
                }
                else
                {
                    LastPopulation.Add(new PopulationSummary
                    {
                        Number = 0,
                        XReal = individual.XRealAfterMutiation,
                        XBin = Utils.real2bin(individual.XRealAfterMutiation, rangeLength, lowerRange, chromosomesNumber),
                        EvaluationFunction = Utils.doEvaluation(individual.XRealAfterMutiation),
                        Percent = 1.0 / EvoAlg.Count * 100
                    });
                }
            }

            LastPopulation = LastPopulation.OrderByDescending(x => x.EvaluationFunction).ToList();

            
            for (int i = 1; i <= LastPopulation.Count; i++)
            {
                LastPopulation.ElementAt(i - 1).Number = i;
                /*
                sb.Append(LastPopulation.ElementAt(i - 1).Number + ";");
                sb.Append(LastPopulation.ElementAt(i - 1).XReal + ";");
                sb.Append(LastPopulation.ElementAt(i - 1).XBin + ";");
                sb.Append(LastPopulation.ElementAt(i - 1).EvaluationFunction + ";");
                sb.AppendLine(LastPopulation.ElementAt(i - 1).Percent + ";");
                File.AppendAllText(filePath, sb.ToString());
                sb.Clear();
                */
            }

            populationSummaryBindingSource.DataSource = LastPopulation;
            populationSummaryBindingSource.ResetBindings(true);

            labelXreal.Text = LastPopulation[0].XReal.ToString();
            labelXbin.Text = LastPopulation[0].XBin.ToString();
            labelFx.Text = LastPopulation[0].EvaluationFunction.ToString();
        }
    }
}
