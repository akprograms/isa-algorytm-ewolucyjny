﻿namespace ISA_Algorytm_Ewolucyjny
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxMutacja = new System.Windows.Forms.TextBox();
            this.textBoxKrzyzowanie = new System.Windows.Forms.TextBox();
            this.textBoxPokolenia = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxPopulacja = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxDokladnosc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxZakresB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxZakresA = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.numberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xRealDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.evaluationFunctionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitnessFunctionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.probabilityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.distributionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.randomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xRealAfterSelectionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.parentXRealDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.parentXBinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cuttingPointDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xRealAfterCrossoverDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xBinAfterCrossoverDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mutationPointDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xBinAfterMutiationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xRealAfterMutiationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.evolutionaryAlgorithmBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.checkBoxElita = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.evolutionaryAlgorithmBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxElita);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.textBoxMutacja);
            this.groupBox1.Controls.Add(this.textBoxKrzyzowanie);
            this.groupBox1.Controls.Add(this.textBoxPokolenia);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxPopulacja);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxDokladnosc);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxZakresB);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxZakresA);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(8, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(931, 118);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parametry";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(688, 21);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 71);
            this.button3.TabIndex = 17;
            this.button3.Text = "Testy";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(769, 22);
            this.button2.Name = "button2";
            this.button2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button2.Size = new System.Drawing.Size(75, 71);
            this.button2.TabIndex = 16;
            this.button2.Text = "Podsumowanie";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButton2);
            this.groupBox2.Controls.Add(this.radioButton1);
            this.groupBox2.Location = new System.Drawing.Point(529, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(121, 100);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Rodzaj funkcji";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 63);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(93, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "Minimalizująca";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 35);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(111, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Maksymalizucjąca";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(850, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 71);
            this.button1.TabIndex = 14;
            this.button1.Text = "Uruchom";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxMutacja
            // 
            this.textBoxMutacja.Location = new System.Drawing.Point(439, 76);
            this.textBoxMutacja.Name = "textBoxMutacja";
            this.textBoxMutacja.Size = new System.Drawing.Size(37, 20);
            this.textBoxMutacja.TabIndex = 13;
            this.textBoxMutacja.Text = "0,005";
            // 
            // textBoxKrzyzowanie
            // 
            this.textBoxKrzyzowanie.Location = new System.Drawing.Point(464, 48);
            this.textBoxKrzyzowanie.Name = "textBoxKrzyzowanie";
            this.textBoxKrzyzowanie.Size = new System.Drawing.Size(37, 20);
            this.textBoxKrzyzowanie.TabIndex = 12;
            this.textBoxKrzyzowanie.Text = "0,8";
            // 
            // textBoxPokolenia
            // 
            this.textBoxPokolenia.Location = new System.Drawing.Point(373, 22);
            this.textBoxPokolenia.Name = "textBoxPokolenia";
            this.textBoxPokolenia.Size = new System.Drawing.Size(37, 20);
            this.textBoxPokolenia.TabIndex = 11;
            this.textBoxPokolenia.Text = "500";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(285, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(148, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Prawdopodobieństwo mutacji:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(285, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(173, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Prawdopodobieństwo krzyżowania:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(285, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Liczba pokoleń:";
            // 
            // textBoxPopulacja
            // 
            this.textBoxPopulacja.Location = new System.Drawing.Point(105, 76);
            this.textBoxPopulacja.Name = "textBoxPopulacja";
            this.textBoxPopulacja.Size = new System.Drawing.Size(37, 20);
            this.textBoxPopulacja.TabIndex = 7;
            this.textBoxPopulacja.Text = "40";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Rozmiar populacji:";
            // 
            // textBoxDokladnosc
            // 
            this.textBoxDokladnosc.Location = new System.Drawing.Point(160, 48);
            this.textBoxDokladnosc.Name = "textBoxDokladnosc";
            this.textBoxDokladnosc.Size = new System.Drawing.Size(24, 20);
            this.textBoxDokladnosc.TabIndex = 5;
            this.textBoxDokladnosc.Text = "3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Dokładność rozwiązania: 10^-";
            // 
            // textBoxZakresB
            // 
            this.textBoxZakresB.Location = new System.Drawing.Point(199, 22);
            this.textBoxZakresB.Name = "textBoxZakresB";
            this.textBoxZakresB.Size = new System.Drawing.Size(37, 20);
            this.textBoxZakresB.TabIndex = 3;
            this.textBoxZakresB.Text = "12";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(174, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "do";
            // 
            // textBoxZakresA
            // 
            this.textBoxZakresA.Location = new System.Drawing.Point(131, 22);
            this.textBoxZakresA.Name = "textBoxZakresA";
            this.textBoxZakresA.Size = new System.Drawing.Size(37, 20);
            this.textBoxZakresA.TabIndex = 1;
            this.textBoxZakresA.Text = "-4";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Przedział rozwiązań: od";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numberDataGridViewTextBoxColumn,
            this.xRealDataGridViewTextBoxColumn,
            this.evaluationFunctionDataGridViewTextBoxColumn,
            this.fitnessFunctionDataGridViewTextBoxColumn,
            this.probabilityDataGridViewTextBoxColumn,
            this.distributionDataGridViewTextBoxColumn,
            this.randomDataGridViewTextBoxColumn,
            this.xRealAfterSelectionDataGridViewTextBoxColumn,
            this.parentXRealDataGridViewTextBoxColumn,
            this.parentXBinDataGridViewTextBoxColumn,
            this.cuttingPointDataGridViewTextBoxColumn,
            this.xRealAfterCrossoverDataGridViewTextBoxColumn,
            this.xBinAfterCrossoverDataGridViewTextBoxColumn,
            this.mutationPointDataGridViewTextBoxColumn,
            this.xBinAfterMutiationDataGridViewTextBoxColumn,
            this.xRealAfterMutiationDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.evolutionaryAlgorithmBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(8, 129);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(931, 307);
            this.dataGridView1.TabIndex = 1;
            // 
            // numberDataGridViewTextBoxColumn
            // 
            this.numberDataGridViewTextBoxColumn.DataPropertyName = "Number";
            this.numberDataGridViewTextBoxColumn.HeaderText = "Number";
            this.numberDataGridViewTextBoxColumn.Name = "numberDataGridViewTextBoxColumn";
            this.numberDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // xRealDataGridViewTextBoxColumn
            // 
            this.xRealDataGridViewTextBoxColumn.DataPropertyName = "XReal";
            this.xRealDataGridViewTextBoxColumn.HeaderText = "XReal";
            this.xRealDataGridViewTextBoxColumn.Name = "xRealDataGridViewTextBoxColumn";
            this.xRealDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // evaluationFunctionDataGridViewTextBoxColumn
            // 
            this.evaluationFunctionDataGridViewTextBoxColumn.DataPropertyName = "EvaluationFunction";
            this.evaluationFunctionDataGridViewTextBoxColumn.HeaderText = "EvaluationFunction";
            this.evaluationFunctionDataGridViewTextBoxColumn.Name = "evaluationFunctionDataGridViewTextBoxColumn";
            this.evaluationFunctionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fitnessFunctionDataGridViewTextBoxColumn
            // 
            this.fitnessFunctionDataGridViewTextBoxColumn.DataPropertyName = "FitnessFunction";
            this.fitnessFunctionDataGridViewTextBoxColumn.HeaderText = "FitnessFunction";
            this.fitnessFunctionDataGridViewTextBoxColumn.Name = "fitnessFunctionDataGridViewTextBoxColumn";
            this.fitnessFunctionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // probabilityDataGridViewTextBoxColumn
            // 
            this.probabilityDataGridViewTextBoxColumn.DataPropertyName = "Probability";
            this.probabilityDataGridViewTextBoxColumn.HeaderText = "Probability";
            this.probabilityDataGridViewTextBoxColumn.Name = "probabilityDataGridViewTextBoxColumn";
            this.probabilityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // distributionDataGridViewTextBoxColumn
            // 
            this.distributionDataGridViewTextBoxColumn.DataPropertyName = "Distribution";
            this.distributionDataGridViewTextBoxColumn.HeaderText = "Distribution";
            this.distributionDataGridViewTextBoxColumn.Name = "distributionDataGridViewTextBoxColumn";
            this.distributionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // randomDataGridViewTextBoxColumn
            // 
            this.randomDataGridViewTextBoxColumn.DataPropertyName = "Random";
            this.randomDataGridViewTextBoxColumn.HeaderText = "Random";
            this.randomDataGridViewTextBoxColumn.Name = "randomDataGridViewTextBoxColumn";
            this.randomDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // xRealAfterSelectionDataGridViewTextBoxColumn
            // 
            this.xRealAfterSelectionDataGridViewTextBoxColumn.DataPropertyName = "XRealAfterSelection";
            this.xRealAfterSelectionDataGridViewTextBoxColumn.HeaderText = "XRealAfterSelection";
            this.xRealAfterSelectionDataGridViewTextBoxColumn.Name = "xRealAfterSelectionDataGridViewTextBoxColumn";
            this.xRealAfterSelectionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // parentXRealDataGridViewTextBoxColumn
            // 
            this.parentXRealDataGridViewTextBoxColumn.DataPropertyName = "ParentXReal";
            this.parentXRealDataGridViewTextBoxColumn.HeaderText = "ParentXReal";
            this.parentXRealDataGridViewTextBoxColumn.Name = "parentXRealDataGridViewTextBoxColumn";
            this.parentXRealDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // parentXBinDataGridViewTextBoxColumn
            // 
            this.parentXBinDataGridViewTextBoxColumn.DataPropertyName = "ParentXBin";
            this.parentXBinDataGridViewTextBoxColumn.HeaderText = "ParentXBin";
            this.parentXBinDataGridViewTextBoxColumn.Name = "parentXBinDataGridViewTextBoxColumn";
            this.parentXBinDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cuttingPointDataGridViewTextBoxColumn
            // 
            this.cuttingPointDataGridViewTextBoxColumn.DataPropertyName = "CuttingPoint";
            this.cuttingPointDataGridViewTextBoxColumn.HeaderText = "CuttingPoint";
            this.cuttingPointDataGridViewTextBoxColumn.Name = "cuttingPointDataGridViewTextBoxColumn";
            this.cuttingPointDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // xRealAfterCrossoverDataGridViewTextBoxColumn
            // 
            this.xRealAfterCrossoverDataGridViewTextBoxColumn.DataPropertyName = "XRealAfterCrossover";
            this.xRealAfterCrossoverDataGridViewTextBoxColumn.HeaderText = "XRealAfterCrossover";
            this.xRealAfterCrossoverDataGridViewTextBoxColumn.Name = "xRealAfterCrossoverDataGridViewTextBoxColumn";
            this.xRealAfterCrossoverDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // xBinAfterCrossoverDataGridViewTextBoxColumn
            // 
            this.xBinAfterCrossoverDataGridViewTextBoxColumn.DataPropertyName = "XBinAfterCrossover";
            this.xBinAfterCrossoverDataGridViewTextBoxColumn.HeaderText = "XBinAfterCrossover";
            this.xBinAfterCrossoverDataGridViewTextBoxColumn.Name = "xBinAfterCrossoverDataGridViewTextBoxColumn";
            this.xBinAfterCrossoverDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mutationPointDataGridViewTextBoxColumn
            // 
            this.mutationPointDataGridViewTextBoxColumn.DataPropertyName = "MutationPoint";
            this.mutationPointDataGridViewTextBoxColumn.HeaderText = "MutationPoint";
            this.mutationPointDataGridViewTextBoxColumn.Name = "mutationPointDataGridViewTextBoxColumn";
            this.mutationPointDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // xBinAfterMutiationDataGridViewTextBoxColumn
            // 
            this.xBinAfterMutiationDataGridViewTextBoxColumn.DataPropertyName = "XBinAfterMutiation";
            this.xBinAfterMutiationDataGridViewTextBoxColumn.HeaderText = "XBinAfterMutiation";
            this.xBinAfterMutiationDataGridViewTextBoxColumn.Name = "xBinAfterMutiationDataGridViewTextBoxColumn";
            this.xBinAfterMutiationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // xRealAfterMutiationDataGridViewTextBoxColumn
            // 
            this.xRealAfterMutiationDataGridViewTextBoxColumn.DataPropertyName = "XRealAfterMutiation";
            this.xRealAfterMutiationDataGridViewTextBoxColumn.HeaderText = "XRealAfterMutiation";
            this.xRealAfterMutiationDataGridViewTextBoxColumn.Name = "xRealAfterMutiationDataGridViewTextBoxColumn";
            this.xRealAfterMutiationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // evolutionaryAlgorithmBindingSource
            // 
            this.evolutionaryAlgorithmBindingSource.DataSource = typeof(EvoAlgorithmLib.EvolutionaryAlgorithm);
            // 
            // checkBoxElita
            // 
            this.checkBoxElita.AutoSize = true;
            this.checkBoxElita.Location = new System.Drawing.Point(177, 79);
            this.checkBoxElita.Name = "checkBoxElita";
            this.checkBoxElita.Size = new System.Drawing.Size(46, 17);
            this.checkBoxElita.TabIndex = 18;
            this.checkBoxElita.Text = "Elita";
            this.checkBoxElita.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 439);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "ISA Algorytm Ewolucyjny";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.evolutionaryAlgorithmBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxMutacja;
        private System.Windows.Forms.TextBox textBoxKrzyzowanie;
        private System.Windows.Forms.TextBox textBoxPokolenia;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxPopulacja;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxDokladnosc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxZakresB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxZakresA;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.DataGridViewTextBoxColumn crossoverDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mutationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn parametersNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn parametersXRealDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn parametersEvaluationFunctionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn selectionFitnessFunctionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn matchFunctionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xRealDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn evaluationFunctionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitnessFunctionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn probabilityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn distributionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn randomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xRealAfterSelectionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn parentXRealDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn parentXBinDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cuttingPointDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xRealAfterCrossoverDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xBinAfterCrossoverDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mutationPointDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xBinAfterMutiationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xRealAfterMutiationDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource evolutionaryAlgorithmBindingSource;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox checkBoxElita;
    }
}

