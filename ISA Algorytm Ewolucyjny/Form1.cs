﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EvoAlgorithmLib;
using System.Reflection;
using System.Threading;

namespace ISA_Algorytm_Ewolucyjny
{
    public partial class Form1 : Form
    {
        public List<EvolutionaryAlgorithm> EvoAlg = new List<EvolutionaryAlgorithm>();
        public List<double> TempGeneration = new List<double>();
        public List<ChartData> ChartData = new List<ChartData>();

        private int rangeLength;
        private int chromosomesNumber;

        public Form1()
        {
            InitializeComponent();
        }

        private List<EvolutionaryAlgorithm> startAlgorithm(int lowerRange, int upperRange, int precision, int populationSize, int generationsNumber, bool functionIsMaximizing, double crossoverChance, double mutationChance, bool eliteOn)
        {
            List<EvolutionaryAlgorithm> EvoAlg = new List<EvolutionaryAlgorithm>();
            double accuracy = Math.Pow(10, precision);
            rangeLength = upperRange > 0 ? upperRange - lowerRange : Math.Abs(lowerRange - upperRange);
            double tmp = rangeLength * accuracy + 1;
            chromosomesNumber = (int)Math.Ceiling(Math.Log(tmp, 2));

            Random rand = new Random();
            double xRealTmp = 0;

            for (int actualGeneration = 0; actualGeneration < generationsNumber; actualGeneration++)
            {
                EvoAlg.Clear();
                for (int i = 1; i <= populationSize; i++)
                {
                    double xReal;
                    if (actualGeneration > 0)
                    {
                        xReal = TempGeneration.ElementAt(i - 1);
                    }
                    else
                    {
                        xReal = Math.Round(rand.NextDouble() * (upperRange - lowerRange) + lowerRange, precision);
                    }
                    
                    double Fx = Utils.doEvaluation(xReal);
                
                    EvoAlg.Add(new EvolutionaryAlgorithm
                    {
                        Number = i,
                        XReal = xReal,
                        EvaluationFunction = Fx
                    });
                }

                // elita - zapisanie najlepszego XReal
                if (eliteOn)
                {
                    xRealTmp = EvoAlg.First(x => x.EvaluationFunction == EvoAlg.Select(y => y.EvaluationFunction).Max()).XReal;
                }

                // selekcja
                double fext;
                if (functionIsMaximizing)
                {
                    fext = EvoAlg.Min(ind => ind.EvaluationFunction);
                }
                else
                {
                    fext = EvoAlg.Max(ind => ind.EvaluationFunction);
                }

                foreach (var individual in EvoAlg)
                {
                    individual.FitnessFunction = Utils.fitness(individual.EvaluationFunction, fext, precision, functionIsMaximizing);
                }

                double sumOfFitnessed = EvoAlg.Sum(f => f.FitnessFunction);
                foreach (var individual in EvoAlg)
                {
                    individual.Probability = Utils.countProbability(individual.FitnessFunction, sumOfFitnessed);
                }

                List<double> probabilities = EvoAlg.Select(s => s.Probability).ToList();
                for (int i = 0; i < populationSize; i++)
                {
                    EvoAlg.ElementAt(i).Distribution = Utils.doDistribution(probabilities, i);
                }

                List<double> weights = EvoAlg.Select(s => s.Distribution).ToList();
                foreach (var individual in EvoAlg)
                {
                    individual.Random = rand.NextDouble();
                    individual.XRealAfterSelection = EvoAlg.Select(x => x.XReal).ElementAt(Utils.select(weights, individual.Random));
                }

                // krzyzowanie
                foreach (var individual in EvoAlg)
                {
                    if(rand.NextDouble() < crossoverChance)
                    {
                        individual.ParentXReal = individual.XRealAfterSelection;
                        individual.ParentXBin = Utils.real2bin(individual.XRealAfterSelection, rangeLength, lowerRange, chromosomesNumber);
                    }
                    else
                    {
                        individual.ParentXBin = "---";
                        individual.XRealAfterCrossover = individual.XRealAfterSelection;
                    }
                }

                List<EvolutionaryAlgorithm> crossIndividuals = EvoAlg.Where(i => i.ParentXReal != 0).ToList();
                Utils.doCrossover(crossIndividuals, chromosomesNumber, lowerRange, rangeLength, precision);

                // mutacja 
                Utils.doMutation(EvoAlg, chromosomesNumber, lowerRange, rangeLength, precision, mutationChance);

                TempGeneration = EvoAlg.Select(x => x.XRealAfterMutiation).ToList();

                // elita - losowe podstawienie
                if (eliteOn)
                {
                    int randElem = rand.Next(0, populationSize - 1);
                    double EvaluationFunctionTmp = Utils.doEvaluation(EvoAlg.ElementAt(randElem).XRealAfterMutiation);

                    if (Utils.doEvaluation(xRealTmp) > EvaluationFunctionTmp) // tylko dla funckji maksymalizujacej! (dla min <)
                    {
                        TempGeneration[randElem] = xRealTmp;
                    }
                }

                ChartData.Add(new ChartData
                {
                    Number = actualGeneration + 1,
                    Max = EvoAlg.Select(x => x.EvaluationFunction).Max(),
                    Avg = EvoAlg.Select(x => x.EvaluationFunction).Average(),
                    Min = EvoAlg.Select(x => x.EvaluationFunction).Min()
                });
            }

            return EvoAlg;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ChartData.Clear();
            EvoAlg = startAlgorithm(Int32.Parse(textBoxZakresA.Text), Int32.Parse(textBoxZakresB.Text), Int32.Parse(textBoxDokladnosc.Text), Int32.Parse(textBoxPopulacja.Text), Int32.Parse(textBoxPokolenia.Text), radioButton1.Checked, Double.Parse(textBoxKrzyzowanie.Text), Double.Parse(textBoxMutacja.Text), checkBoxElita.Checked);
            evolutionaryAlgorithmBindingSource.DataSource = EvoAlg;
            evolutionaryAlgorithmBindingSource.ResetBindings(true);
            button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SummaryForm form = new SummaryForm(ChartData, EvoAlg, rangeLength, Int32.Parse(textBoxZakresA.Text), chromosomesNumber);
            form.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            /*
            int repeats = 100;

            int[] numberOfIndividuals = new int[11]; numberOfIndividuals[0] = 30; // start 30 koniec 80 skok 5 || 11 wartosci
            int[] numberOfGenerations = new int[11]; numberOfGenerations[0] = 50; // start 50 koniec 150 skok 10 || 11 wartosci
            double[] crossoverProbability = new double[9]; crossoverProbability[0] = 0.5; // start 0.5 koniec 0.9 skok 0.05 || 9 wartosci
            double[] mutationProbability = new double[21]; mutationProbability[0] = 0.0001; // start 0.0001 koniec 0.01 skok 0.0005 || 21 wartosci

            for (int i = 1; i < numberOfIndividuals.Length; i++) numberOfIndividuals[i] = numberOfIndividuals[0] + 5 * i;
            for (int i = 1; i < numberOfGenerations.Length; i++) numberOfGenerations[i] = numberOfGenerations[0] + 10 * i;
            for (int i = 1; i < crossoverProbability.Length; i++) crossoverProbability[i] = crossoverProbability[0] + 0.05 * i;
            for (int i = 1; i < mutationProbability.Length; i++) mutationProbability[i] = 0.0005 * i;

            List<Thread> calculationThreads = new List<Thread>();

            // 2 286 900 powtórzeń
            for (int n = 0; n < numberOfIndividuals.Length; n++)
            {
                calculationThreads.Add(new Thread(new Test
                {
                    NumberOfIndividuals = numberOfIndividuals[n],
                    NumberOfGenerations = numberOfGenerations,
                    CrossoverProbability = crossoverProbability,
                    MutationProbability = mutationProbability,
                    Repeats = repeats,
                    LowerRange = Int32.Parse(textBoxZakresA.Text),
                    UpperRange = Int32.Parse(textBoxZakresB.Text),
                    Precision = Int32.Parse(textBoxDokladnosc.Text),
                    IsMaximizing = radioButton1.Checked,
                    CallEvolutionaryAlgorithm = startAlgorithm
                }.Calculate));

                calculationThreads.ElementAt(n).Start();
                while (!calculationThreads.ElementAt(n).IsAlive);

            }
            */

            TestsForm form = new TestsForm(Int32.Parse(textBoxZakresA.Text), Int32.Parse(textBoxZakresB.Text), Int32.Parse(textBoxDokladnosc.Text), radioButton1.Checked); // , startAlgorithm
            form.Show();

            //calculationThreads.ElementAt(1);

            /*
            while (calculationThreads.Any(x => x.IsAlive))
            {
                // ekran postępu?
            }
            */
            // pokaz wyniki/info o zakonczeniu pracy
        }
    }
}
