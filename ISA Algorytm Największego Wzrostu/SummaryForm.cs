﻿using EvoAlgorithmLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ISA_Algorytm_Największego_Wzrostu
{
    public partial class SummaryForm : Form
    {
        public SummaryForm(List<LargestGrowthAlgorithm> ChartLGA, List<LargestGrowthAlgorithm> BestLGA, int iterations)
        {
            InitializeComponent();
            largestGrowthAlgorithmChartBindingSource.DataSource = ChartLGA;
            largestGrowthAlgorithmChartBindingSource.ResetBindings(true);
            largestGrowthAlgorithmSecondChartBindingSource.DataSource = BestLGA;
            largestGrowthAlgorithmSecondChartBindingSource.ResetBindings(true);

            chartOfLGA.Series.Clear();

            var tmp = BestLGA.Select(x => x.Iteration).ToList();

            int tmpPos = 1;
            for (int i = 0; i < tmp.Count; i++)
            {
                Series series = chartOfLGA.Series.Add(tmp.ElementAt(i).ToString());
                series.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                series.Color = Color.Red;
                series.IsVisibleInLegend = false;

                var tmp2 = ChartLGA.Where(y => y.Iteration == i).Select(x => x.EvaluationFunction).ToArray();
                for (int j = 0; j < tmp2.Length; j++)
                {
                    series.Points.AddXY(tmpPos, tmp2[j]); // i + (1.0 / tmp2.Length) * j
                    tmpPos++;
                }

            }

            chartOfBest.Series.Clear();

            Series seriesBest = chartOfBest.Series.Add("best");
            seriesBest.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            seriesBest.Color = Color.Blue;
            seriesBest.IsVisibleInLegend = false;

            double actualMax = BestLGA.ElementAt(0).EvaluationFunction;
            seriesBest.Points.AddXY(1, actualMax);
            for (int i = 2; i <= iterations; i++)
            {
                if(BestLGA.ElementAt(i-1).EvaluationFunction > actualMax)
                {
                    actualMax = BestLGA.ElementAt(i - 1).EvaluationFunction;
                }
                seriesBest.Points.AddXY(i, actualMax);
            }

            labelLocals.Text = tmpPos.ToString();
            LargestGrowthAlgorithm best = BestLGA.First(x => x.EvaluationFunction == BestLGA.Select(y => y.EvaluationFunction).Max());
            labelXreal.Text = best.XReal.ToString();
            labelXbin.Text = best.XBin.ToString();
            labelFx.Text = best.EvaluationFunction.ToString();

            chartOfLGA.Update();
            chartOfBest.Update();

            
        }
    }
}
