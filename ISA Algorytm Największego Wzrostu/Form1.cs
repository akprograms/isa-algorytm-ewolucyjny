﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EvoAlgorithmLib;
using System.Windows.Forms.DataVisualization.Charting;

namespace ISA_Algorytm_Największego_Wzrostu
{
    public partial class Form1 : Form
    {
        private List<LargestGrowthAlgorithm> BestLGA = new List<LargestGrowthAlgorithm>();
        private List<LargestGrowthAlgorithm> ChartLGA = new List<LargestGrowthAlgorithm>();

        public Form1()
        {
            InitializeComponent();
            largestGrowthAlgorithmBindingSource.DataSource = BestLGA;
        }

        private void startAlgotihm(int lowerRange, int upperRange, int precision, int numberOfIterations)
        {
            int actualIteration = 0;
            bool endIteration;
            double accuracy = Math.Pow(10, precision);
            int rangeLength = upperRange > 0 ? upperRange - lowerRange : Math.Abs(lowerRange - upperRange);
            double tmp = rangeLength * accuracy + 1;
            int chromosomesNumber = (int)Math.Ceiling(Math.Log(tmp, 2));
            Random rand = new Random();
            double xRealTmp;
            List<LargestGrowthAlgorithm> newChains = new List<LargestGrowthAlgorithm>();

            do
            {
                endIteration = false;
                xRealTmp = Math.Round(rand.NextDouble() * (upperRange - lowerRange) + lowerRange, precision);
                BestLGA.Add(new LargestGrowthAlgorithm
                {
                    Iteration = actualIteration + 1,
                    XReal = xRealTmp,
                    XBin = Utils.real2bin(xRealTmp, rangeLength, lowerRange, chromosomesNumber),
                    EvaluationFunction = Utils.doEvaluation(xRealTmp)
                });

                do
                {
                    newChains = Utils.createNewChains(BestLGA.ElementAt(actualIteration).XBin);
                    foreach (var chain in newChains)
                    {
                        chain.Iteration = actualIteration + 1;
                        chain.XReal = Utils.bin2real(chain.XBin, rangeLength, lowerRange, chromosomesNumber, precision);
                        chain.EvaluationFunction = Utils.doEvaluation(chain.XReal);
                    }

                    if (BestLGA.ElementAt(actualIteration).EvaluationFunction < newChains.Select(x => x.EvaluationFunction).Max())
                    {
                        BestLGA[actualIteration] = newChains.First(x => x.EvaluationFunction == newChains.Select(y => y.EvaluationFunction).Max());
                    }
                    else
                    {
                        endIteration = true;
                    }
                    ChartLGA.Add(BestLGA[actualIteration]);
                } while (!endIteration);
                actualIteration++;
            } while (actualIteration < numberOfIterations);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            BestLGA.Clear();
            ChartLGA.Clear();
            startAlgotihm(Int32.Parse(textBoxZakresA.Text), Int32.Parse(textBoxZakresB.Text), Int32.Parse(textBoxDokladnosc.Text), Int32.Parse(textBoxIteracje.Text));
            largestGrowthAlgorithmBindingSource.ResetBindings(true);

            //chartOfLGA.Series.Add("LGA");
            //chartOfLGA.Series["LGA"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;

            button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SummaryForm form = new SummaryForm(ChartLGA, BestLGA, Int32.Parse(textBoxIteracje.Text));
            form.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TestsForm form = new TestsForm(Int32.Parse(textBoxZakresA.Text), Int32.Parse(textBoxZakresB.Text), Int32.Parse(textBoxDokladnosc.Text), Int32.Parse(textBoxIteracje.Text));
            form.Show();
        }
    }
}
