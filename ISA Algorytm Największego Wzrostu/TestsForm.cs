﻿using EvoAlgorithmLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ISA_Algorytm_Największego_Wzrostu
{
    public partial class TestsForm : Form
    {
        private int lowerRange { get; set; }
        private int upperRange { get; set; }
        private int precision { get; set; }
        private int numberOfIterations { get; set; }

        private List<LGATest> Tests = new List<LGATest>();
        private List<LargestGrowthAlgorithm> BestLGA = new List<LargestGrowthAlgorithm>();
        private List<LargestGrowthAlgorithm> Results = new List<LargestGrowthAlgorithm>();

        public TestsForm(int lowerRange, int upperRange, int precision, int numberOfIterations)
        {
            this.lowerRange = lowerRange;
            this.upperRange = upperRange;
            this.precision = precision;
            this.numberOfIterations = numberOfIterations;

            Tests.Clear();
            Results.Clear();
            BestLGA.Clear();

            InitializeComponent();
            lGATestBindingSource.DataSource = Tests;
            lGATestChartBindingSource.DataSource = Tests;

            runTest();

            lGATestBindingSource.ResetBindings(true);
            lGATestChartBindingSource.ResetBindings(true);
        }

        private bool check(double fx, double max)
        {
            double tmp = max - max * 0.05;

            if (fx > tmp)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void runTest()
        {
            double MaxValue = 1.997;
            bool resultFound;
            int actualIteration = 0;
            bool endIteration;
            double accuracy = Math.Pow(10, precision);
            int rangeLength = upperRange > 0 ? upperRange - lowerRange : Math.Abs(lowerRange - upperRange);
            double tmp = rangeLength * accuracy + 1;
            int chromosomesNumber = (int)Math.Ceiling(Math.Log(tmp, 2));
            Random rand = new Random();
            double xRealTmp;
            List<LargestGrowthAlgorithm> newChains = new List<LargestGrowthAlgorithm>();

            for (int testID = 1; testID <= 1000; testID++)
            {
                resultFound = false;

                actualIteration = 0;
                accuracy = Math.Pow(10, precision);
                rangeLength = upperRange > 0 ? upperRange - lowerRange : Math.Abs(lowerRange - upperRange);
                tmp = rangeLength * accuracy + 1;
                chromosomesNumber = (int)Math.Ceiling(Math.Log(tmp, 2));
                rand = new Random();
                newChains = new List<LargestGrowthAlgorithm>();

                do
                {
                    endIteration = false;
                    xRealTmp = Math.Round(rand.NextDouble() * (upperRange - lowerRange) + lowerRange, precision);
                    BestLGA.Add(new LargestGrowthAlgorithm
                    {
                        Iteration = actualIteration + 1,
                        XReal = xRealTmp,
                        XBin = Utils.real2bin(xRealTmp, rangeLength, lowerRange, chromosomesNumber),
                        EvaluationFunction = Utils.doEvaluation(xRealTmp)
                    });

                    do
                    {
                        newChains = Utils.createNewChains(BestLGA.ElementAt(actualIteration).XBin);
                        foreach (var chain in newChains)
                        {
                            chain.Iteration = actualIteration + 1;
                            chain.XReal = Utils.bin2real(chain.XBin, rangeLength, lowerRange, chromosomesNumber, precision);
                            chain.EvaluationFunction = Utils.doEvaluation(chain.XReal);
                        }

                        if (BestLGA.ElementAt(actualIteration).EvaluationFunction < newChains.Select(x => x.EvaluationFunction).Max())
                        {
                            BestLGA[actualIteration] = newChains.First(x => x.EvaluationFunction == newChains.Select(y => y.EvaluationFunction).Max());
                        }
                        else
                        {
                            endIteration = true;
                        }

                        if(check(BestLGA.ElementAt(actualIteration).EvaluationFunction, MaxValue))
                        {
                            Results.Add(new LargestGrowthAlgorithm
                            {
                                Iteration = actualIteration + 1,
                                EvaluationFunction = BestLGA.ElementAt(actualIteration).EvaluationFunction
                            });
                            resultFound = true;
                            break;
                        }
                        //ChartLGA.Add(BestLGA[actualIteration]);
                    } while (!endIteration);

                    if(resultFound)
                    {
                        BestLGA.Clear();
                        break;
                    }

                    actualIteration++;
                } while (actualIteration < 100);
            }

            for (int iter = 1; iter <= 100; iter++)
            {
                int numberOfSolutions = Results.Where(x => x.Iteration == iter).Count();

                int successCumulation = iter > 1 ? numberOfSolutions + Tests.Select(x => x.NumberOfSolutions).Sum() : numberOfSolutions;

                double percent = Math.Round(((double)successCumulation / 1000) * 100, 1);

                Tests.Add(new LGATest
                {
                    NumberOfIterations = iter,
                    NumberOfSolutions = numberOfSolutions,
                    SuccessCumulation = successCumulation,
                    Percent = percent
                });

                if (successCumulation == 1000)
                {
                    break;
                }
            }

            testsChart.Series.Clear();
            Series series = testsChart.Series.Add("new");
            series.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series.Color = Color.Red;
            series.IsVisibleInLegend = false;

            foreach (var item in Tests)
            {
                series.Points.AddXY(item.NumberOfIterations, item.Percent);
            }

            testsChart.Update();

        }
    }
}
