﻿namespace ISA_Algorytm_Największego_Wzrostu
{
    partial class SummaryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelFx = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelXbin = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelXreal = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.largestGrowthAlgorithmChartBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.chartOfLGA = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartOfBest = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label2 = new System.Windows.Forms.Label();
            this.labelLocals = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.largestGrowthAlgorithmSecondChartBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.largestGrowthAlgorithmChartBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartOfLGA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartOfBest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.largestGrowthAlgorithmSecondChartBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelFx);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.labelXbin);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.labelXreal);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(712, 91);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Najlepsze rozwiązanie";
            // 
            // labelFx
            // 
            this.labelFx.AutoSize = true;
            this.labelFx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFx.Location = new System.Drawing.Point(51, 64);
            this.labelFx.Name = "labelFx";
            this.labelFx.Size = new System.Drawing.Size(24, 20);
            this.labelFx.TabIndex = 5;
            this.labelFx.Text = "---";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(6, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "f(x): ";
            // 
            // labelXbin
            // 
            this.labelXbin.AutoSize = true;
            this.labelXbin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelXbin.Location = new System.Drawing.Point(206, 31);
            this.labelXbin.Name = "labelXbin";
            this.labelXbin.Size = new System.Drawing.Size(24, 20);
            this.labelXbin.TabIndex = 3;
            this.labelXbin.Text = "---";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(151, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Xbin: ";
            // 
            // labelXreal
            // 
            this.labelXreal.AutoSize = true;
            this.labelXreal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelXreal.Location = new System.Drawing.Point(63, 31);
            this.labelXreal.Name = "labelXreal";
            this.labelXreal.Size = new System.Drawing.Size(24, 20);
            this.labelXreal.TabIndex = 1;
            this.labelXreal.Text = "---";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Xreal: ";
            // 
            // largestGrowthAlgorithmChartBindingSource
            // 
            this.largestGrowthAlgorithmChartBindingSource.DataSource = typeof(EvoAlgorithmLib.LargestGrowthAlgorithm);
            // 
            // chartOfLGA
            // 
            chartArea1.Name = "ChartArea1";
            this.chartOfLGA.ChartAreas.Add(chartArea1);
            this.chartOfLGA.DataSource = this.largestGrowthAlgorithmChartBindingSource;
            legend1.Name = "Legend1";
            this.chartOfLGA.Legends.Add(legend1);
            this.chartOfLGA.Location = new System.Drawing.Point(12, 122);
            this.chartOfLGA.Name = "chartOfLGA";
            this.chartOfLGA.Size = new System.Drawing.Size(712, 205);
            this.chartOfLGA.TabIndex = 3;
            this.chartOfLGA.Text = "chart1";
            // 
            // chartOfBest
            // 
            chartArea2.Name = "ChartArea1";
            this.chartOfBest.ChartAreas.Add(chartArea2);
            this.chartOfBest.DataSource = this.largestGrowthAlgorithmSecondChartBindingSource;
            legend2.Name = "Legend1";
            this.chartOfBest.Legends.Add(legend2);
            this.chartOfBest.Location = new System.Drawing.Point(12, 349);
            this.chartOfBest.Name = "chartOfBest";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartOfBest.Series.Add(series1);
            this.chartOfBest.Size = new System.Drawing.Size(712, 227);
            this.chartOfBest.TabIndex = 4;
            this.chartOfBest.Text = "chart2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(196, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Wykres iteracji lokalnych, liczba iteracji: ";
            // 
            // labelLocals
            // 
            this.labelLocals.AutoSize = true;
            this.labelLocals.Location = new System.Drawing.Point(215, 106);
            this.labelLocals.Name = "labelLocals";
            this.labelLocals.Size = new System.Drawing.Size(16, 13);
            this.labelLocals.TabIndex = 6;
            this.labelLocals.Text = "---";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 332);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Wykres V best";
            // 
            // largestGrowthAlgorithmSecondChartBindingSource
            // 
            this.largestGrowthAlgorithmSecondChartBindingSource.DataSource = typeof(EvoAlgorithmLib.LargestGrowthAlgorithm);
            // 
            // SummaryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 577);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelLocals);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chartOfBest);
            this.Controls.Add(this.chartOfLGA);
            this.Controls.Add(this.groupBox1);
            this.Name = "SummaryForm";
            this.Text = "Podsumowanie";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.largestGrowthAlgorithmChartBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartOfLGA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartOfBest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.largestGrowthAlgorithmSecondChartBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelFx;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelXbin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelXreal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource largestGrowthAlgorithmChartBindingSource;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartOfLGA;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartOfBest;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelLocals;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.BindingSource largestGrowthAlgorithmSecondChartBindingSource;
    }
}