﻿namespace ISA_Algorytm_Największego_Wzrostu
{
    partial class TestsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.testsChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lGATestBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.numberOfIterationsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numberOfSolutionsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.successCumulationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.percentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lGATestChartBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testsChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lGATestBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lGATestChartBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numberOfIterationsDataGridViewTextBoxColumn,
            this.numberOfSolutionsDataGridViewTextBoxColumn,
            this.successCumulationDataGridViewTextBoxColumn,
            this.percentDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.lGATestBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(701, 226);
            this.dataGridView1.TabIndex = 0;
            // 
            // testsChart
            // 
            chartArea1.Name = "ChartArea1";
            this.testsChart.ChartAreas.Add(chartArea1);
            this.testsChart.DataSource = this.lGATestChartBindingSource;
            legend1.Name = "Legend1";
            this.testsChart.Legends.Add(legend1);
            this.testsChart.Location = new System.Drawing.Point(12, 244);
            this.testsChart.Name = "testsChart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.testsChart.Series.Add(series1);
            this.testsChart.Size = new System.Drawing.Size(701, 246);
            this.testsChart.TabIndex = 1;
            this.testsChart.Text = "chart1";
            // 
            // lGATestBindingSource
            // 
            this.lGATestBindingSource.DataSource = typeof(EvoAlgorithmLib.LGATest);
            // 
            // numberOfIterationsDataGridViewTextBoxColumn
            // 
            this.numberOfIterationsDataGridViewTextBoxColumn.DataPropertyName = "NumberOfIterations";
            this.numberOfIterationsDataGridViewTextBoxColumn.HeaderText = "NumberOfIterations";
            this.numberOfIterationsDataGridViewTextBoxColumn.Name = "numberOfIterationsDataGridViewTextBoxColumn";
            this.numberOfIterationsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // numberOfSolutionsDataGridViewTextBoxColumn
            // 
            this.numberOfSolutionsDataGridViewTextBoxColumn.DataPropertyName = "NumberOfSolutions";
            this.numberOfSolutionsDataGridViewTextBoxColumn.HeaderText = "NumberOfSolutions";
            this.numberOfSolutionsDataGridViewTextBoxColumn.Name = "numberOfSolutionsDataGridViewTextBoxColumn";
            this.numberOfSolutionsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // successCumulationDataGridViewTextBoxColumn
            // 
            this.successCumulationDataGridViewTextBoxColumn.DataPropertyName = "SuccessCumulation";
            this.successCumulationDataGridViewTextBoxColumn.HeaderText = "SuccessCumulation";
            this.successCumulationDataGridViewTextBoxColumn.Name = "successCumulationDataGridViewTextBoxColumn";
            this.successCumulationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // percentDataGridViewTextBoxColumn
            // 
            this.percentDataGridViewTextBoxColumn.DataPropertyName = "Percent";
            this.percentDataGridViewTextBoxColumn.HeaderText = "Percent";
            this.percentDataGridViewTextBoxColumn.Name = "percentDataGridViewTextBoxColumn";
            this.percentDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lGATestChartBindingSource
            // 
            this.lGATestChartBindingSource.DataSource = typeof(EvoAlgorithmLib.LGATest);
            // 
            // TestsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(725, 490);
            this.Controls.Add(this.testsChart);
            this.Controls.Add(this.dataGridView1);
            this.Name = "TestsForm";
            this.Text = "Testy";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testsChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lGATestBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lGATestChartBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataVisualization.Charting.Chart testsChart;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberOfIterationsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberOfSolutionsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn successCumulationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn percentDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource lGATestBindingSource;
        private System.Windows.Forms.BindingSource lGATestChartBindingSource;
    }
}