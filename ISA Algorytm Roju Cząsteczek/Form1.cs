﻿using EvoAlgorithmLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ISA_Algorytm_Roju_Cząsteczek
{
    public partial class Form1 : Form
    {
        private List<Particle> ParticlesData = new List<Particle>();

        public Form1()
        {
            InitializeComponent();
            particleBindingSource.DataSource = ParticlesData;
        }

        private List<Particle> startAlgorithm(int lowerRange, int upperRange, int precision, int maxIterations, int particlesNumber, int neighborhoodSize, double weightC1, double weightC2, double weightC3)
        {
            List<Particle> Particles = new List<Particle>();
            button1.Text = "W toku...";
            button1.Enabled = false;
            button2.Enabled = false;

            int actualIteration = 0;
            double accuracy = Math.Pow(10, precision);
            int rangeLength = upperRange > 0 ? upperRange - lowerRange : Math.Abs(lowerRange - upperRange);
            double tmp = rangeLength * accuracy + 1;
            int chromosomesNumber = (int)Math.Ceiling(Math.Log(tmp, 2));
            int neighborsNumber = (int)Math.Round((neighborhoodSize / 100.0) * particlesNumber);
            Random rand = new Random();
            double tmpPos;

            ChartArea ca = chart1.ChartAreas[0];
            Series S1 = chart1.Series[0];

            // no y-axis:            
            ca.AxisY.Enabled = AxisEnabled.False;
            ca.AxisY.Minimum = 0;
            ca.AxisY.Maximum = 1;

            // use your own values:
            ca.AxisX.Minimum = lowerRange;
            ca.AxisX.Maximum = upperRange;

            // style the ticks, use your own values:
            ca.AxisX.MajorTickMark.Size = 7;
            ca.AxisX.MajorTickMark.Interval = 10;
            ca.AxisX.MinorTickMark.Enabled = true;
            ca.AxisX.MinorTickMark.Size = 3;
            ca.AxisX.MinorTickMark.Interval = 2;

            // I turn the axis labels off.
            ca.AxisX.LabelStyle.Enabled = false;
            // If you want to show them pick a reasonable Interval!
            ca.AxisX.Interval = 1;

            // no gridlines
            ca.AxisY.MajorGrid.Enabled = false;
            ca.AxisX.MajorGrid.Enabled = false;

            // the most logical type
            // note that you can change colors, sizes, shaps and markerstyles..
            S1.ChartType = SeriesChartType.Point;

            for (int i = 0; i < particlesNumber; i++)
            {
                tmpPos = Math.Round(rand.NextDouble() * (upperRange - lowerRange) + lowerRange, precision);
                Particles.Add(new Particle
                {
                    Number = i + 1,
                    Position = tmpPos,
                    BestSelfKnownPosition = tmpPos,
                    MoveVector = 0
                });
            }

            List<Particle> neighbors = new List<Particle>();
            int neighborsLeft = neighborsNumber;
            int level = 1;

            do
            {
                // obliczanie najlepszej pozycji w sasiedztwie
                foreach (var particle in Particles)
                {
                    if (neighborhoodSize < 100)
                    {
                        neighbors.Clear();
                        while (neighborsLeft > 0)
                        {
                            if (!(Particles.IndexOf(particle) - level < 0))
                            {
                                // biorę lewego
                                neighbors.Add(Particles.ElementAt(Particles.IndexOf(particle) - level));
                            }
                            else
                            {
                                for (int j = 0; j < neighborsLeft; j++)
                                {
                                    neighbors.Add(Particles.ElementAt(Particles.IndexOf(particle) + level + j));
                                }
                                break;
                            }
                            neighborsLeft--;
                            if ((!(Particles.IndexOf(particle) + level >= Particles.Count)) && (neighborsLeft > 0))
                            {
                                // biorę prawego
                                neighbors.Add(Particles.ElementAt(Particles.IndexOf(particle) + level));
                            }
                            else
                            {
                                level++;
                                for (int j = 0; j < neighborsLeft; j++)
                                {
                                    neighbors.Add(Particles.ElementAt(Particles.IndexOf(particle) - level - j));
                                }
                                break;
                            }
                            neighborsLeft--;
                            level++;
                        }
                        level = 1;
                        neighborsLeft = neighborsNumber;
                    }
                    else
                    {
                        neighbors = Particles;
                    }

                    foreach (var neighbor in neighbors)
                    {
                        neighbor.EvaluationFunction = Utils.doEvaluation(neighbor.Position);
                    }

                    particle.BestNeighborhoodKnownPosition = neighbors.First(x => x.EvaluationFunction == neighbors.Select(y => y.EvaluationFunction).Max()).Position;
                }

                foreach (var particle in Particles)
                {
                    // Oblicz wartość funkcji celu: f(xi)
                    particle.EvaluationFunction = Utils.doEvaluation(particle.Position);
                    // IF(f(xi) > f(bi)) THEN bi = xi
                    if (particle.EvaluationFunction > Utils.doEvaluation(particle.BestSelfKnownPosition))
                    {
                        particle.BestSelfKnownPosition = particle.Position;
                    }
                    // IF(f(xi) > f(bg) THEN bg = xi
                    if (particle.EvaluationFunction > Utils.doEvaluation(particle.BestNeighborhoodKnownPosition))
                    {
                        particle.BestNeighborhoodKnownPosition = particle.Position;
                    }
                }

                foreach (var particle in Particles)
                {
                    // Oblicz wartość wektora prędkości: vi(t + 1)
                    // vit + 1 = c1r1vit + c2r2(bit - xit) + c3r3(bgt - xit)
                    particle.MoveVector = (weightC1 * rand.NextDouble() * particle.MoveVector) +
                        (weightC2 * rand.NextDouble() * (particle.BestSelfKnownPosition - particle.Position)) +
                        (weightC3 * rand.NextDouble() * (particle.BestNeighborhoodKnownPosition - particle.Position));
                    // Oblicz pozycję cząstki: xi(t + 1)
                    // xit + 1 = xit + vit + 1
                    particle.Position = particle.Position + particle.MoveVector;

                }

                // rysowanie aktualnych pozycji czasteczek + opoznienie
                S1.Points.Clear();
                foreach (var particle in Particles)
                {
                    S1.Points.AddXY(particle.Position, 0.1);
                }
                chart1.Update();
                Thread.Sleep(250);

                actualIteration++;
            } while (actualIteration < maxIterations); // || najmniejszy błąd nie zostanie osiągnięty

            button1.Text = "Uruchom";
            button1.Enabled = true;

            return Particles;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ParticlesData.Clear();
            ParticlesData = startAlgorithm(Int32.Parse(textBoxZakresA.Text), Int32.Parse(textBoxZakresB.Text), Int32.Parse(textBoxDokladnosc.Text), Int32.Parse(textBoxIteracje.Text), Int32.Parse(textBoxCzasteczki.Text), trackBar1.Value, Double.Parse(textBoxWaga1.Text), Double.Parse(textBoxWaga2.Text), Double.Parse(textBoxWaga3.Text));
            particleBindingSource.ResetBindings(true);
            button2.Enabled = true;
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            sliderLabel.Text = trackBar1.Value + " %";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SummaryForm form = new SummaryForm(ParticlesData);
            form.Show();
        }
    }
}
