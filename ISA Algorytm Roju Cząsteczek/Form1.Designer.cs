﻿namespace ISA_Algorytm_Roju_Cząsteczek
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.sliderLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.textBoxCzasteczki = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxWaga3 = new System.Windows.Forms.TextBox();
            this.textBoxWaga2 = new System.Windows.Forms.TextBox();
            this.textBoxWaga1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxIteracje = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxDokladnosc = new System.Windows.Forms.TextBox();
            this.textBoxZakresA = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxZakresB = new System.Windows.Forms.TextBox();
            this.particleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.particleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.trackBar1);
            this.groupBox1.Controls.Add(this.sliderLabel);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.textBoxCzasteczki);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.textBoxWaga3);
            this.groupBox1.Controls.Add(this.textBoxWaga2);
            this.groupBox1.Controls.Add(this.textBoxWaga1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxIteracje);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxDokladnosc);
            this.groupBox1.Controls.Add(this.textBoxZakresA);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxZakresB);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(702, 120);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parametry";
            // 
            // trackBar1
            // 
            this.trackBar1.LargeChange = 10;
            this.trackBar1.Location = new System.Drawing.Point(378, 76);
            this.trackBar1.Maximum = 100;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(163, 45);
            this.trackBar1.TabIndex = 29;
            this.trackBar1.Value = 30;
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // sliderLabel
            // 
            this.sliderLabel.AutoSize = true;
            this.sliderLabel.Location = new System.Drawing.Point(493, 60);
            this.sliderLabel.Name = "sliderLabel";
            this.sliderLabel.Size = new System.Drawing.Size(30, 13);
            this.sliderLabel.TabIndex = 28;
            this.sliderLabel.Text = "30 %";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(381, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "Rozmiar sąsiedztwa: ";
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(567, 70);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(129, 44);
            this.button2.TabIndex = 26;
            this.button2.Text = "Podsumowanie";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBoxCzasteczki
            // 
            this.textBoxCzasteczki.Location = new System.Drawing.Point(488, 28);
            this.textBoxCzasteczki.Name = "textBoxCzasteczki";
            this.textBoxCzasteczki.Size = new System.Drawing.Size(53, 20);
            this.textBoxCzasteczki.TabIndex = 25;
            this.textBoxCzasteczki.Text = "100";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(381, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Liczba cząsteczek: ";
            // 
            // textBoxWaga3
            // 
            this.textBoxWaga3.Location = new System.Drawing.Point(310, 86);
            this.textBoxWaga3.Name = "textBoxWaga3";
            this.textBoxWaga3.Size = new System.Drawing.Size(41, 20);
            this.textBoxWaga3.TabIndex = 23;
            this.textBoxWaga3.Text = "0,2";
            // 
            // textBoxWaga2
            // 
            this.textBoxWaga2.Location = new System.Drawing.Point(310, 57);
            this.textBoxWaga2.Name = "textBoxWaga2";
            this.textBoxWaga2.Size = new System.Drawing.Size(41, 20);
            this.textBoxWaga2.TabIndex = 22;
            this.textBoxWaga2.Text = "0,3";
            // 
            // textBoxWaga1
            // 
            this.textBoxWaga1.Location = new System.Drawing.Point(310, 28);
            this.textBoxWaga1.Name = "textBoxWaga1";
            this.textBoxWaga1.Size = new System.Drawing.Size(41, 20);
            this.textBoxWaga1.TabIndex = 21;
            this.textBoxWaga1.Text = "0,5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(250, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Waga c3: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(250, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Waga c2:  ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(250, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Waga c1: ";
            // 
            // textBoxIteracje
            // 
            this.textBoxIteracje.Location = new System.Drawing.Point(83, 86);
            this.textBoxIteracje.Name = "textBoxIteracje";
            this.textBoxIteracje.Size = new System.Drawing.Size(38, 20);
            this.textBoxIteracje.TabIndex = 17;
            this.textBoxIteracje.Text = "50";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(567, 15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 44);
            this.button1.TabIndex = 0;
            this.button1.Text = "Uruchom";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Liczba iteracji";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Przedzial rozwiązań: od";
            // 
            // textBoxDokladnosc
            // 
            this.textBoxDokladnosc.Location = new System.Drawing.Point(160, 57);
            this.textBoxDokladnosc.Name = "textBoxDokladnosc";
            this.textBoxDokladnosc.Size = new System.Drawing.Size(24, 20);
            this.textBoxDokladnosc.TabIndex = 15;
            this.textBoxDokladnosc.Text = "3";
            // 
            // textBoxZakresA
            // 
            this.textBoxZakresA.Location = new System.Drawing.Point(129, 28);
            this.textBoxZakresA.Name = "textBoxZakresA";
            this.textBoxZakresA.Size = new System.Drawing.Size(34, 20);
            this.textBoxZakresA.TabIndex = 11;
            this.textBoxZakresA.Text = "-4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Dokładność rozwiązania: 10^-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(169, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "do";
            // 
            // textBoxZakresB
            // 
            this.textBoxZakresB.Location = new System.Drawing.Point(194, 28);
            this.textBoxZakresB.Name = "textBoxZakresB";
            this.textBoxZakresB.Size = new System.Drawing.Size(34, 20);
            this.textBoxZakresB.TabIndex = 13;
            this.textBoxZakresB.Text = "12";
            // 
            // particleBindingSource
            // 
            this.particleBindingSource.DataSource = typeof(EvoAlgorithmLib.Particle);
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Location = new System.Drawing.Point(13, 139);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(701, 125);
            this.chart1.TabIndex = 1;
            this.chart1.Text = "chart1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 276);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "ISA Algorytm Roju Cząsteczek";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.particleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBoxCzasteczki;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxWaga3;
        private System.Windows.Forms.TextBox textBoxWaga2;
        private System.Windows.Forms.TextBox textBoxWaga1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxIteracje;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxDokladnosc;
        private System.Windows.Forms.TextBox textBoxZakresA;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxZakresB;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label sliderLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.BindingSource particleBindingSource;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
    }
}

