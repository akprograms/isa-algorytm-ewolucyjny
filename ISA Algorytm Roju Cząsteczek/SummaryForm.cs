﻿using EvoAlgorithmLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISA_Algorytm_Roju_Cząsteczek
{
    public partial class SummaryForm : Form
    {
        public SummaryForm(List<Particle> Particles)
        {
            InitializeComponent();

            particleBindingSource.DataSource = Particles;
            particleBindingSource.ResetBindings(true);

            Particle best = Particles.First(x => x.EvaluationFunction == Particles.Select(y => y.EvaluationFunction).Max());

            labelPosition.Text = best.Position.ToString();
            labelFx.Text = best.EvaluationFunction.ToString();
        }
    }
}
