﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvoAlgorithmLib
{
    public class LGATest
    {
        public int NumberOfIterations { get; set; }
        public int NumberOfSolutions { get; set; }
        public int SuccessCumulation { get; set; }
        public double Percent { get; set; }
    }
}
