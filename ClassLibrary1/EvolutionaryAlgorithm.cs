﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvoAlgorithmLib
{
    public class EvolutionaryAlgorithm
    {
        public int Number { get; set; }
        public double XReal { get; set; }
        public double EvaluationFunction { get; set; }
        public double FitnessFunction { get; set; }
        public double Probability { get; set; }
        public double Distribution { get; set; }
        public double Random { get; set; }
        public double XRealAfterSelection { get; set; }
        public double ParentXReal { get; set; }
        public string ParentXBin { get; set; }
        public int CuttingPoint { get; set; }
        public double XRealAfterCrossover { get; set; }
        public string XBinAfterCrossover { get; set; }
        public string MutationPoint { get; set; }
        public string XBinAfterMutiation { get; set; }
        public double XRealAfterMutiation { get; set; }
    }
}
