﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvoAlgorithmLib
{
    public static class Utils
    {
        public static int real2int(double xReal, int rangeLength, int lowerRange, int chromosomesNumber)
        {
            return (int)Math.Round((1.0 / rangeLength) * (xReal - lowerRange) * (Math.Pow(2, chromosomesNumber) - 1));
        }

        public static string int2bin(int xInt, int chromosomesNumber)
        {
            StringBuilder buildString = new StringBuilder();
            string xBin = Convert.ToString(Convert.ToInt64(xInt), 2);
            if (xBin.Length < chromosomesNumber)
            {
                buildString.Clear();
                buildString.Append(xBin);
                while (buildString.Length < chromosomesNumber)
                {
                    buildString.Insert(0, "0");
                }
                xBin = buildString.ToString();
            }
            return xBin;
        }

        public static int bin2int(string xBin)
        {
            return Convert.ToInt32(Convert.ToString(Convert.ToInt64(xBin, 2), 10));
        }

        public static double int2real(int xInt, int rangeLength, int lowerRange, int chromosomesNumber, int precision)
        {
            return Math.Round((xInt * rangeLength) / (Math.Pow(2, chromosomesNumber) - 1) + lowerRange, precision);
        }

        public static string real2bin(double xReal, int rangeLength, int lowerRange, int chromosomesNumber)
        {
            return int2bin(real2int(xReal, rangeLength, lowerRange, chromosomesNumber), chromosomesNumber);
        }

        public static double bin2real(string xBin, int rangeLength, int lowerRange, int chromosomesNumber, int precision)
        {
            return int2real(bin2int(xBin), rangeLength, lowerRange, chromosomesNumber, precision);
        }

        public static double doEvaluation(double xReal)
        {
            return xReal % 1 * (Math.Cos(20 * Math.PI * xReal) - Math.Sin(xReal));
        }

        public static double fitness(double Fx, double Fext, int precision, bool isMaximizing)
        {
            if(isMaximizing)
            {
                return Fx - Fext + Math.Pow(10, -(precision));
            }
            else
            {
                return -(Fx - Fext) + Math.Pow(10, -(precision));
            }
        }

        public static double countProbability(double actualFitnessed, double sumOfFitnessed)
        {
            return actualFitnessed / sumOfFitnessed;
        }

        public static double doDistribution(List<double> probabilities, int index)
        {
            double value = 0;
            for (int i = 0; i <= index; i++)
            {
                value += probabilities.ElementAt(i);
            }
            return value;
        }

        public static int select(List<double> weights, double rand)
        {
            for (int i = 0; i < weights.Count; i++)
            {
                if (rand <= weights.ElementAt(i)) return i;
            }

            return weights.Count - 1;
        }

        public static void doCrossover(List<EvolutionaryAlgorithm> evoAlg, int chromosomesNumber, int lowerRange, int rangeLength, int precision)
        {
            Random rand = new Random();
            for (int i = 0; i < evoAlg.Count; i+=2)
            {
                evoAlg.ElementAt(i).CuttingPoint = rand.Next(1, chromosomesNumber - 1);
                if (i + 1 < evoAlg.Count)
                {
                    evoAlg.ElementAt(i + 1).CuttingPoint = evoAlg.ElementAt(i).CuttingPoint;
                    String parentA = real2bin(evoAlg.ElementAt(i).ParentXReal, rangeLength, lowerRange, chromosomesNumber);
                    String parentB = real2bin(evoAlg.ElementAt(i + 1).ParentXReal, rangeLength, lowerRange, chromosomesNumber);
                    //Console.WriteLine("parentA : " + parentA);
                    //Console.WriteLine("parentB : " + parentB);
                    String childA = parentA.Substring(0, evoAlg.ElementAt(i).CuttingPoint) + parentB.Substring(evoAlg.ElementAt(i).CuttingPoint);
                    String childB = parentB.Substring(0, evoAlg.ElementAt(i).CuttingPoint) + parentA.Substring(evoAlg.ElementAt(i).CuttingPoint);
                    //Console.WriteLine("childA : " + childA);
                    //Console.WriteLine("childB : " + childB);
                    evoAlg.ElementAt(i).XBinAfterCrossover = childA;
                    evoAlg.ElementAt(i + 1).XBinAfterCrossover = childB;
                    evoAlg.ElementAt(i).XRealAfterCrossover = bin2real(childA, rangeLength, lowerRange, chromosomesNumber, precision);
                    evoAlg.ElementAt(i + 1).XRealAfterCrossover = bin2real(childB, rangeLength, lowerRange, chromosomesNumber, precision);
                }
                else
                {
                    evoAlg.ElementAt(i).XRealAfterCrossover = evoAlg.ElementAt(i).ParentXReal;
                }
            }
        }

        public static void doMutation(List<EvolutionaryAlgorithm> evoAlg, int chromosomesNumber, int lowerRange, int rangeLength, int precision, double mutationChance)
        {
            Random rand = new Random();
            foreach (var individual in evoAlg)
            {
                StringBuilder bin = new StringBuilder(real2bin(individual.XRealAfterCrossover, rangeLength, lowerRange, chromosomesNumber));
                //Console.WriteLine("(N)bin = " + bin.ToString());
                for (int i = 0; i < bin.Length; i++)
                {
                    if(rand.NextDouble() < mutationChance)
                    {
                        //Console.WriteLine("MAMMA MIA! MUTACJA! miejsce: " + (i + 1));
                        if (bin[i] == '0') bin.Replace("0", "1", i, 1);
                        else bin.Replace("1", "0", i, 1);
                        individual.MutationPoint += (i + 1) + ",";
                    }
                }
                //Console.WriteLine("(M)bin = " + bin.ToString());
                if (individual.MutationPoint == null)
                {
                    individual.MutationPoint = "---";
                    individual.XBinAfterMutiation = "---";
                    individual.XRealAfterMutiation = individual.XRealAfterCrossover;
                }
                else
                {
                    individual.XBinAfterMutiation = bin.ToString();
                    individual.XRealAfterMutiation = bin2real(bin.ToString(), rangeLength, lowerRange, chromosomesNumber, precision);
                }
                //Console.WriteLine("-------------");
            }
        }

        public static List<LargestGrowthAlgorithm> createNewChains(string chain)
        {
            List<LargestGrowthAlgorithm> newChains = new List<LargestGrowthAlgorithm>();
            StringBuilder bin = new StringBuilder();

            for (int i = 0; i < chain.Length; i++)
            {
                bin.Append(chain);
                
                newChains.Add(new LargestGrowthAlgorithm
                {
                    XBin = bin[i] == '0' ? bin.Replace("0", "1", i, 1).ToString() : bin.Replace("1", "0", i, 1).ToString()
                });

                bin.Clear();
            }

            return newChains;
        }
        
    }
}
