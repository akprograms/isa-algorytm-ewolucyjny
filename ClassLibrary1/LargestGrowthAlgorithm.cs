﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvoAlgorithmLib
{
    public class LargestGrowthAlgorithm
    {
        public int Iteration { get; set; }
        public double XReal { get; set; }
        public string XBin { get; set; }
        public double EvaluationFunction { get; set; }
    }
}
