﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvoAlgorithmLib
{
    public class Particle
    {
        public int Number { get; set; }
        public double Position { get; set; }
        public double EvaluationFunction { get; set; }
        public double BestSelfKnownPosition { get; set; }
        public double BestNeighborhoodKnownPosition { get; set; }
        public double MoveVector { get; set; }
    }
}
