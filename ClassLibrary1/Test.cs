﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvoAlgorithmLib
{
    public class Test
    {
        public int NumberOfIndividuals { get; set; }
        public int[] NumberOfGenerations { get; set; }
        public double[] CrossoverProbability { get; set; }
        public double[] MutationProbability { get; set; }
        public int Repeats { get; set; }

        public int LowerRange { get; set; }
        public int UpperRange { get; set; }
        public int Precision { get; set; }
        public bool IsMaximizing { get; set; }

        public delegate List<EvolutionaryAlgorithm> CallEvoAlg(int lowerRange, int upperRange, int precision, int populationSize, int generationsNumber, bool functionIsMaximizing, double crossoverChance, double mutationChance);

        public CallEvoAlg CallEvolutionaryAlgorithm { get; set; }

        private List<EvolutionaryAlgorithm> EvoAlg = new List<EvolutionaryAlgorithm>();

        public void Calculate()
        {
            for (int t = 0; t < NumberOfGenerations.Length; t++)
            {
                for (int pk = 0; pk < CrossoverProbability.Length; pk++)
                {
                    for (int pm = 0; pm < MutationProbability.Length; pm++)
                    {
                        for (int i = 0; i < Repeats; i++)
                        {
                            // 207 900 powtórzeń
                            // Console.WriteLine("Powtarzam " + i + " dla: numberOfIndividuals = " + NumberOfIndividuals + ", numberOfGenerations = " + NumberOfGenerations[t] + ", crossoverProbability = " + CrossoverProbability[pk] + ", mutationProbability = " + MutationProbability[pm]);

                            // algorytm ewolucyjny
                            EvoAlg = CallEvolutionaryAlgorithm(LowerRange, UpperRange, Precision, NumberOfIndividuals, NumberOfGenerations[t], IsMaximizing, CrossoverProbability[pk], MutationProbability[pm]);
                            // zapisanie wyników do pliku
                            Console.WriteLine("Cos");
                        }
                    }
                }
            }
        }

    }
}
