﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvoAlgorithmLib
{
    public class ChartData
    {
        public int Number { get; set; }
        public double Max { get; set; }
        public double Avg { get; set; }
        public double Min { get; set; }
    }
}
