﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvoAlgorithmLib
{
    [Serializable]
    public class Parameters
    {
        public int Number { get; set; }
        public double XReal { get; set; }
        public int XInt { get; set; }
        public string XBin { get; set; }
        public int XInt2 { get; set; }
        public double XReal2 { get; set; }
        public double Fx { get; set; }

        public Parameters()
        {

        }

        public Parameters(int Number, double XReal, int XInt, string XBin, int XInt2, double XReal2, double Fx)
        {
            this.Number = Number;
            this.XReal = XReal;
            this.XInt = XInt;
            this.XBin = XBin;
            this.XInt2 = XInt2;
            this.XReal2 = XReal2;
            this.Fx = Fx;
        }
    }
}
